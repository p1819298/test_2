<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dessinateur;

class DessinateurController extends Controller
{
    public function index()
    {
        $dessinateurs = Dessinateur::getAll();
        return view('dessinateurs', compact('dessinateurs'));
    }
}
